'use strict';

let fs   = require('fs');
let path = require('path');
let gulp = require('gulp');

let helper = require('../helper');

module.exports = function (projectDir, options) {
    let results = helper.scanWorkingDirs(projectDir, /src$/);

    if (gulp.args.filter) {
        results = helper.filter(results, gulp.args.filter.split(','));
    }

    return function () {
        results.map(result => {
            gulp.watch([result + '/**/*.scss'], [options.scss]);
            gulp.watch([result + '/**/*.js'], [options.js]);
        })
    };
};
