let fs = require('fs');
let path = require('path');
let Stream = require('stream');

module.exports.classify = function (word) {
    word = word.charAt(0).toUpperCase() + word.substr(1);

    while (word.match(/[_\-\s]/)) {
        word = word.replace(/[_\-\s]./g, match => match.substr(1).toUpperCase());
    }

    return word;
};

/**
 * @public
 * @param module
 *
 * @return {*}
 */
module.exports.require = function (module) {
    let relativeRoot = path.relative(process.cwd(), path.join(__dirname));

    return require(path.join(relativeRoot, module));
};

/**
 * Do nothing streaming
 *
 * @return {Stream}
 */
module.exports.noopStream = function () {
    let stream = new Stream.Transform({ objectMode: true });

    stream._transform = function (file, unused, callback) {
        callback(null, file);
    };

    return stream;
};

module.exports.scanWorkingDirs = function scanWorkingDirs(cwd, pattern, results = []) {
    let items = fs.readdirSync(cwd);

    items.forEach(function (item) {
        let fullPath = path.join(cwd, item);
        let itemInfo = fs.statSync(fullPath);

        if (pattern.test(fullPath)) {
            results.push(fullPath);
        } else if (itemInfo.isDirectory()) {
            scanWorkingDirs(fullPath, pattern, results);
        }
    });

    return results;
};

module.exports.filter = function filter(results, filters) {
    return results.filter(item => {
        for (let filter of filters) {
            if (-1 !== item.indexOf(path.sep + filter + path.sep)) {
                return true;
            }
        }

        return false;
    });
};

/**
 * @param {Object} input
 *
 * @return string
 */
module.exports.toArgv = function (input) {
    let argv = '';

    for (let key in input) {
        argv += ' --' + key + '="' + (input[key] || '') + '"';
    }

    return argv;
};

//// Boostrap break-point calculator
// tables = [];
// for (var screenSize = 500; screenSize <= 2560; screenSize++) {
//     let columnSize = screenSize * 8.33333 / 100;
//     let remainder = columnSize % Math.floor(columnSize);
//
//     if (0 === remainder || remainder >= 0.99991 || remainder <= 0.00001) {
//         tables.push({screen_size: screenSize, column_size: columnSize, remainder: remainder});
//     }
// }
//
// console.table(tables);
