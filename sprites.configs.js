'use strict';

module.exports = function (projectDir) {
    return {
        imgName: projectDir + '/public/images/sprites.png',
        imgPath: '../images/sprites.png',
        cssFormat: 'css',
        cssName: projectDir + '/src/scss/elements/_sprites.scss',
        cssTemplate: './sprites.handlebars',
        cssHandlebarsHelpers: {
            pixel: function (input) {
                return 0 === parseInt(input) ? 0 : input;
            }
        }
    };
};
