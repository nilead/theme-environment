SystemJS.config({
  paths: {
    "github:": "jspm_packages/github/",
    "npm:": "jspm_packages/npm/"
  },
  browserConfig: {
    "paths": {
      "lead/": "/src/"
    }
  },
  nodeConfig: {
    "paths": {
      "lead/": "src/"
    }
  },
  transpiler: false,
  map: {
    "jquery/jquery": "github:jquery/jquery@3.2.1",
    "bootstrap@4.0.0-alpha.3": "npm:bootstrap@4.0.0-alpha.3",
  },
  packages: {
    "lead": {
      "main": "lead.js"
    },
    "npm:bootstrap@4.0.0-alpha.3": {
      "map": {
        "jquery": "npm:jquery@3.2.1",
        "tether": "github:HubSpot/tether@1.4.0"
      }
    }
  }
});

SystemJS.config({
  packageConfigPaths: [
    "npm:@*/*.json",
    "npm:*.json",
    "github:*/*.json"
  ],
  map: {
    "bootstrap": "github:twbs/bootstrap@3.3.7",
    "bootstrap@4.0.0-alpha.2": "github:twbs/bootstrap@4.0.0-alpha.2",
    "bootstrap@4.0.0-alpha.4": "github:twbs/bootstrap@4.0.0-alpha.4",
    "bootstrap@4.0.0-alpha.6": "github:twbs/bootstrap@4.0.0-alpha.6",
    "hamburgers": "npm:hamburgers@0.9.1",
    "hammerjs": "npm:hammerjs@2.0.8",
    "jquery": "npm:jquery@3.2.1",
    "jquery@1.12.4": "npm:jquery@1.12.4",
    "jquery@2.2.4": "npm:jquery@2.2.4",
    "jquery@3.2.1": "npm:jquery@3.2.1",
    "lodash": "npm:lodash@4.17.4",
    "path": "npm:jspm-nodelibs-path@0.2.3",
    "process": "npm:jspm-nodelibs-process@0.2.1",
    "tether": "github:HubSpot/tether@1.4.0"
  },
  packages: {
    "github:twbs/bootstrap@3.3.7": {
      "map": {
        "jquery": "npm:jquery@3.2.1"
      }
    },
    "github:twbs/bootstrap@4.0.0-alpha.2": {
      "map": {
        "jquery": "npm:jquery@2.2.4",
        "tether": "github:HubSpot/tether@1.4.0"
      }
    },
    "npm:hamburgers@0.9.1": {
      "map": {
        "systemjs-json": "github:systemjs/plugin-json@0.1.2"
      }
    },
    "npm:hammerjs@2.0.8": {
      "map": {}
    },
    "npm:jquery@1.12.4": {
      "map": {}
    },
    "github:twbs/bootstrap@4.0.0-alpha.6": {
      "map": {
        "jquery": "npm:jquery@2.2.4",
        "tether": "github:HubSpot/tether@1.4.0"
      }
    },
    "github:twbs/bootstrap@4.0.0-alpha.4": {
      "map": {
        "jquery": "npm:jquery@2.2.4",
        "tether": "github:HubSpot/tether@1.4.0"
      }
    }
  }
});
